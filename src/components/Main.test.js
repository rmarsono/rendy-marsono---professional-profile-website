import React from 'react'
import ReactDOM from 'react-dom'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

import Main from './Main'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Main />, div)
  ReactDOM.unmountComponentAtNode(div)
})

describe('main content', () => {
  it('loads Main text', () => {
    const wrapper = shallow(<Main />)
    const text = wrapper.find('.content-wrap').text()
    expect(text).toEqual('Main content')
  })
  it('matches the snapshot', () => {
    const tree = renderer.create(<Main />).toJSON()
    expect(tree).toMatchSnapshot()
  })
})
