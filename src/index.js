import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import * as serviceWorker from './serviceWorker'
import Header from './components/Header'
import Footer from './components/Footer'
import Main from './components/Main'
import Chat from './components/Chat'
import Thoughts from './components/Thoughts'
import NotFound from './components/NotFound'

import './styles/styles.scss'

const router = (
  <BrowserRouter>
    <React.Fragment>
      <Header />
      <Switch>
        <Route exact path='/' component={Main} />
        <Route exact path='/thoughts' component={Thoughts} />
        <Route exact path='/chat' component={Chat} />
        <Route component={NotFound} />
      </Switch>
      <Footer />
    </React.Fragment>
  </BrowserRouter>
)

ReactDOM.render(router, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
